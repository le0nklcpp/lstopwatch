/*
     This file is part of LStopWatch.

    LStopWatch is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

    LStopWatch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with LStopWatch. If not, see <https://www.gnu.org/licenses/>.
*/
#include "mainwindow.h"
#include "./ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->lcd->display("00:00:00.00");
    count.callOnTimeout([this]{this->timerThink();});
    count.setInterval(10);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::timerThink()
{
     #define twodigit(n) (n>=10?QString::number(n):QString("0")+QString::number(n))
     #define colon(n) (QString(":")+twodigit(n))
     clock.update(); // so basically it just updates indicators
     ui->lcd->display(twodigit(clock.hr)+colon(clock.min)+colon(clock.sec)+QString(".")+twodigit(clock.msec));
}
void MainWindow::startTimer()
{
     ui->start->setText(QString("Stop"));
     clock.start();
     count.start();
}
void MainWindow::stopTimer()
{
     ui->start->setText(QString("Start"));
     count.stop();
}
void MainWindow::on_start_clicked(bool checked)
{
     if(checked)startTimer();
     else stopTimer();
}

void MainWindow::on_record_clicked()
{
    if(ui->start->isChecked())
    {
        ui->listWidget->addItem(QString::number(clock.hr)+":"+QString::number(clock.min)+":"+QString::number(clock.sec)+":"+QString::number(clock.msec));
    }
}


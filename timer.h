/*
     This file is part of LStopWatch.

    LStopWatch is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

    LStopWatch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with LStopWatch. If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef TIMER_H
#define TIMER_H
#include <time.h>
struct lTimer
{
 unsigned char msec; // 0-100
 unsigned char sec; // 0-60
 unsigned char min; // 0-60
 unsigned char hr; // 0-24
 time_t timestamp;
 timespec getTime();
 void update();
 void start();
 lTimer():msec(0),sec(0),min(0){start();}
};
#endif // TIMER_H

/*
     This file is part of LStopWatch.

    LStopWatch is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or any later version.

    LStopWatch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

    You should have received a copy of the GNU General Public License along with LStopWatch. If not, see <https://www.gnu.org/licenses/>.
*/
#include <timer.h>
#include <iostream>
void lTimer::start()
{
    msec = sec = min = hr = 0;
    timestamp = getTime().tv_sec;
}
void lTimer::update()
{
    timespec t = getTime();
    int deltasec = t.tv_sec-timestamp;
    msec = t.tv_nsec/1000000/10; // milliseconds/10
    sec = deltasec%60;
    min = (deltasec/60)%60;
    hr = (deltasec/60/60)%60;
}
timespec lTimer::getTime()
{
    timespec t;
    clock_gettime(CLOCK_MONOTONIC,&t);
    return t;
}
